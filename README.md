# Computer processor components

My implementation of some computer processor components, along with their
output.


# Why I made this programm

I made this programm as part of my first year university computer processors
module. I found it so interesting to see how the logic worked in real world
application.


# Frameworks used

    * Hardware_simulator

# Features

    * Computer processor components with their outputs. These outputs
        were created using a hardware simulator which tests component
        scripts against the expected output.
    
    
# How to run on Linux

To run these pieces of code, a hardware_simulator will be required. When on the
simulator, load the scripts and a test script into the program and you will
be shown the output. (The outputs are already included in the .cmp files)
    

# Working Examples

The hardware simulator we used:

![hardwaresim](/images/hardware_simulator.png)


# Specification

[Link 1: Coursework 1 specification](https://gitlab.com/lawebb/computer-processor-components/-/blob/master/specification/comp1212_coursework_1.pdf)

[Link 2: Coursework 2 specification](https://gitlab.com/lawebb/computer-processor-components/-/blob/master/specification/comp1212_coursework_2.pdf)